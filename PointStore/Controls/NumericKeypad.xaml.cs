﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PointStore
{
    /// <summary>
    /// Interaction logic for NumericKeypad.xaml
    /// </summary>
    public partial class NumericKeypad : UserControl
    {
        public delegate void ValueChangedHandler(object sender, String value);

        public ValueChangedHandler ValueChanged;

        public String cumulativeValue;

        public NumericKeypad()
        {
            InitializeComponent();
            Reset();
        }

        public void Reset()
        {
            if (cumulativeValue != null && cumulativeValue.Length > 0)
            {
                cumulativeValue = String.Empty;
                this.OnValueChanged();
            }
        }

        private void onNumericButtonClick(object sender, RoutedEventArgs e)
        {
            String clickedValue = ((Button)sender).Content.ToString();
            cumulativeValue += clickedValue;

            this.OnValueChanged();
        }

        private void onDeleteButtonClick(object sender, RoutedEventArgs e)
        {
            if (cumulativeValue.Length > 0)
            {
                cumulativeValue = cumulativeValue.Remove(cumulativeValue.Length - 1);
                this.OnValueChanged();
            }
        }

        private void OnValueChanged()
        {
            if (ValueChanged != null)
            {
                ValueChanged(this, cumulativeValue);
            }
        }

        private void onClearButtonClick(object sender, RoutedEventArgs e)
        {
            Reset();
        }
    }
}
