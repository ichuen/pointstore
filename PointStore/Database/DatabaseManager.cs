﻿using System;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;

namespace PointStore.Database
{
    public sealed class DatabaseManager
    {
        private static String DB_FILENAME = "PointStore.sqlite";
        private static String databaseFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), DatabaseManager.DB_FILENAME);
        private SQLiteConnection conn;

        // Singleton instance.  Make sure this is after the other fields have been initialized.
        private static DatabaseManager instance;

        private DatabaseManager()
        {
            SetupDB();
        }

        public static String DatabaseFilePath
        {
            get { return DatabaseManager.databaseFilePath; }
            set { DatabaseManager.databaseFilePath = value; }
        }

        public static DatabaseManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DatabaseManager();
                }

                return instance;
            }
        }

        private void SetupDB()
        {
            try
            {
                Debug.WriteLine(String.Format("DB File Path = {0}", databaseFilePath));

                if (!File.Exists(databaseFilePath))
                {
                    Debug.WriteLine("Created new DB File.");
                    SQLiteConnection.CreateFile(databaseFilePath);
                }

                conn = new SQLiteConnection(String.Format("Data Source={0};Version=3;", databaseFilePath));
                conn.Open();

                // Create the settings table.
                string sql = "CREATE TABLE IF NOT EXISTS setting (key varchar(50) PRIMARY KEY, value varchar(50) NOT NULL)";
                ExecuteNonQuery(sql);

                // Create the core customer table.
                sql = "CREATE TABLE IF NOT EXISTS customer (customer_id INTEGER PRIMARY KEY, created DATETIME DEFAULT CURRENT_TIMESTAMP, updated DATETIME DEFAULT CURRENT_TIMESTAMP, phone_number varchar(20) UNIQUE, points INT, visits INT)";
                ExecuteNonQuery(sql);

                // Create the customer activity table.
                sql = "CREATE TABLE IF NOT EXISTS customer_activity (created DATETIME DEFAULT CURRENT_TIMESTAMP, customer_id INTEGER NOT NULL, point_change INT NOT NULL, action_type INT NOT NULL, FOREIGN KEY(customer_id) REFERENCES customer(customer_id))";
                ExecuteNonQuery(sql);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                throw e;
            }
        }

        public void CloseDatabase()
        {
            if (conn != null) {
                conn.Close();
            }

            instance = null;
        }

        public void ExecuteNonQuery(String sql)
        {
            using (SQLiteCommand command = new SQLiteCommand(sql, conn))
            {
                command.ExecuteNonQuery();
            }
        }

        public object ExecuteScalar(String sql)
        {
            using (SQLiteCommand command = new SQLiteCommand(sql, conn))
            {
                object result = command.ExecuteScalar();
                return result;
            }
        }

        public SQLiteDataReader ExecuteReader(String sql)
        {
            using (SQLiteCommand command = new SQLiteCommand(sql, conn))
            {
                SQLiteDataReader reader = command.ExecuteReader();
                return reader;
            }
        }

        public long ExecuteScalarLong(String sql)
        {
            object result = this.ExecuteScalar(sql);
            try
            {
                return (long)result;
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public string ExecuteScalarString(String sql)
        {
            object result = this.ExecuteScalar(sql);
            try
            {
                return (string)result;
            }
            catch (Exception e)
            {
                return null;
            }
        }

    }
}
