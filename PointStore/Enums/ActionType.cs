﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PointStore.Enums
{
    public enum ActionType : int
    {
        AddPoint = 0,
        RemovePoint = 1,
        RedeemPoints = 2
    }
}
