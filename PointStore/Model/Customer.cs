﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PointStore.Database;
using PointStore.Enums;

namespace PointStore.Model
{
    public class Customer
    {
        private long id;
        public DateTime created;
        public DateTime updated;
        public long phoneNumber;
        public int points;
        public int visits;

        public void UpdatePoints(int pointChange, ActionType actionType)
        {
            points += pointChange;
            this.Update();

            // Record the activity
            CustomerActivity activity = new CustomerActivity(this.id, pointChange, actionType);
            activity.Save();
        }

        public Customer()
        {
        }

        public Customer(long phoneNumber)
        {
            this.phoneNumber = phoneNumber;
        }

        public static Customer GetByPhoneNumber(long phoneNumber)
        {
            string sql = String.Format("select * from customer where phone_number = '{0}'", phoneNumber.ToString());
            SQLiteDataReader reader = DatabaseManager.Instance.ExecuteReader(sql);
            
            Customer c = null;
            if (reader.HasRows)
            {
                reader.Read();
                c = new Customer();
                c.id = (long)reader["customer_id"];
                c.created = (DateTime)reader["created"];
                c.updated = (DateTime)reader["updated"];
                string pn = (string)reader["phone_number"];
                c.phoneNumber = long.Parse(pn);
                c.points = (int)reader["points"];
                c.visits = (int)reader["visits"];
            }

            return c;
        }

        public void Save()
        {
            string sql = String.Format("insert into customer (phone_number, points, visits) values ('{0}', {1}, {2})", 
                this.phoneNumber.ToString(), this.points, this.visits);
            DatabaseManager.Instance.ExecuteNonQuery(sql);

            // Reload from database and update local properties.
            Customer c = Customer.GetByPhoneNumber(this.phoneNumber);
            this.Copy(c);
        }

        public void Update()
        {
            string sql = String.Format("update customer set points = {0}, visits = {1}, updated = CURRENT_TIMESTAMP where phone_number = '{2}'", 
                this.points, this.visits, this.phoneNumber.ToString());
            DatabaseManager.Instance.ExecuteNonQuery(sql);
        }

        private void Copy(Customer c)
        {
            this.id = c.id;
            this.created = c.created;
            this.updated = c.updated;
            this.phoneNumber = c.phoneNumber;
            this.points = c.points;
            this.visits = c.visits;
        }
    }
}
