﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PointStore.Database;
using PointStore.Enums;

namespace PointStore.Model
{
    class CustomerActivity
    {
        private long id;
        public DateTime created;
        public long customerId;
        public int pointChange;
        public ActionType actionType;

        public CustomerActivity() { }

        public CustomerActivity(long customerId, int pointChange, ActionType actionType)
        {
            this.customerId = customerId;
            this.pointChange = pointChange;
            this.actionType = actionType;
        }

        public void Save() {
            string sql = String.Format("insert into customer_activity (customer_id, point_change, action_type) values ({0}, {1}, {2})",
                this.customerId, this.pointChange, (int)this.actionType);
            DatabaseManager.Instance.ExecuteNonQuery(sql);
        }
    }
}
