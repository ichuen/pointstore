﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PointStore.Database;

namespace PointStore.Model
{
    class Setting
    {
        public enum SettingKey
        {
            PointsForRedemption
        }

        private static Dictionary<SettingKey, String> SettingKeyStrings = new Dictionary<SettingKey, string>() { 
            {SettingKey.PointsForRedemption, "pointsForRedemption"}
        };

        private static Dictionary<SettingKey, String> defaultSettings = new Dictionary<SettingKey, string> {
            {SettingKey.PointsForRedemption, "10"}
        };

        private static Dictionary<SettingKey, String> settings = new Dictionary<SettingKey, String>();

        public static String GetSetting(SettingKey key)
        {
            if (settings.ContainsKey(key))
            {
                // Get from cache if it exists.
                return (String)settings[key];
            }
            else
            {
                String sql = String.Format("SELECT value FROM setting where key = '{0}'", SettingKeyStrings[key]);
                String value = DatabaseManager.Instance.ExecuteScalarString(sql);

                // Save the default setting into the DB if not in DB yet.
                if (value == null)
                {
                    SaveSetting(key, defaultSettings[key]);
                    value = defaultSettings[key];
                }

                // Save to cache.
                settings[key] = value;

                return value;
            }
        }

        public static int GetSettingInt(SettingKey key)
        {
            String value = GetSetting(key);
            return int.Parse(value);
        }

        public static void SaveSetting(SettingKey key, String value)
        {
            string sql = String.Format("insert into setting (key, value) values ('{0}', {1})",
                SettingKeyStrings[key], value);
            DatabaseManager.Instance.ExecuteNonQuery(sql);

            // Save to cache
            settings[key] = value;
        }

        public static void UpdateSetting(SettingKey key, String value)
        {
            string sql = String.Format("update setting set value='{0}' where key='{1}'",
                value, SettingKeyStrings[key]);
            DatabaseManager.Instance.ExecuteNonQuery(sql);

            // Save to cache
            settings[key] = value;
        }

        public static void UpdateSettingInt(SettingKey key, int value)
        {
            UpdateSetting(key, value.ToString());
        }
    }
}
