﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PointStore.Database;
using PointStore.Model;
using PointStore.Utils;

namespace PointStore
{
    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    public partial class HomePage : Page
    {
        private static int MaxPhoneNumberLength = 10;
        private static int AddButtonsRowIndex = 1;
        private GridLength addButtonsRowHeight;

        public HomePage()
        {
            InitializeComponent();
            this.numericKeypad.ValueChanged += new NumericKeypad.ValueChangedHandler(onValueChanged);

            // Store the default height of the add buttons row for later use.
            this.addButtonsRowHeight = this.gridOuter.RowDefinitions[AddButtonsRowIndex].Height;

            gridOuter.RowDefinitions[AddButtonsRowIndex].Height = new GridLength(0);

            ResetPage();
        }

        private void ResetPage()
        {
            this.numericKeypad.IsEnabled = true;
            this.numericKeypad.Reset();

            this.txtPhoneNumber.Text = String.Empty;

            if (gridOuter.RowDefinitions[AddButtonsRowIndex].Height.Value != 0)
            {
                GridLengthAnimation gla = new GridLengthAnimation();
                gla.From = addButtonsRowHeight;
                gla.To = new GridLength(0);
                gla.Duration = new TimeSpan(0, 0, 0, 0, 200);
                gridOuter.RowDefinitions[AddButtonsRowIndex].BeginAnimation(RowDefinition.HeightProperty, gla);
            }
        }

        private void onValueChanged(object sender, String value)
        {
            this.txtPhoneNumber.Text = CommonUtils.GeneratePhoneString(value);

            if (value.Length >= MaxPhoneNumberLength)
            {
                Customer c = Customer.GetByPhoneNumber(long.Parse(value));

                if (c != null)
                {
                    // Accessing the user's account counts as a visit.
                    c.visits++;
                    c.Update();
                    this.NavigationService.Navigate(new UserProfilePage(c));
                }
                else
                {
                    this.txtPhoneNumber.Text = "User not found!";
                    
                    this.numericKeypad.IsEnabled = false;

                    GridLengthAnimation gla = new GridLengthAnimation();
                    gla.From = new GridLength(0);
                    gla.To = addButtonsRowHeight;
                    gla.Duration = new TimeSpan(0, 0, 0, 0, 200);
                    gridOuter.RowDefinitions[AddButtonsRowIndex].BeginAnimation(RowDefinition.HeightProperty, gla);

                }
            }
        }

        private void onPageLoaded(object sender, RoutedEventArgs e)
        {
            ResetPage();
            this.numericKeypad.Reset();
        }

        private void onPageUnloaded(object sender, RoutedEventArgs e)
        {

        }

        private void onAddUserClick(object sender, RoutedEventArgs e)
        {
            String value = this.numericKeypad.cumulativeValue;
            Customer c = new Customer(long.Parse(value));

            // Accessing the user's account counts as 1 visit.
            c.visits = 1;
            c.Save();
            this.NavigationService.Navigate(new UserProfilePage(c));
        }

        private void onShowStatsButtonClick(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new StatsPage());
        }

        private void onCancelClick(object sender, RoutedEventArgs e)
        {
            ResetPage();
        }
    }
}
