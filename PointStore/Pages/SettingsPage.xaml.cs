﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PointStore.Model;

namespace PointStore.Pages
{
    /// <summary>
    /// Interaction logic for SettingsPage.xaml
    /// </summary>
    public partial class SettingsPage : Page
    {
        private static int MinRedeemPoints = 1;
        private static int MaxRedeemPoints = 50;

        public SettingsPage()
        {
            InitializeComponent();
            updateRedeemPoints(0);
        }

        private void btnPlusRedeemPoints_Click(object sender, RoutedEventArgs e)
        {
            updateRedeemPoints(1);
        }

        private void btnMinusRedeemPoints_Click(object sender, RoutedEventArgs e)
        {
            updateRedeemPoints(-1);
        }

        private void updateRedeemPoints(int incrementalValue)
        {
            int currentValue = Setting.GetSettingInt(Setting.SettingKey.PointsForRedemption);
            int newValue = currentValue + incrementalValue;

            if (incrementalValue != 0)
            {
                if (newValue >= MinRedeemPoints && newValue <= MaxRedeemPoints)
                {
                    Setting.UpdateSettingInt(Setting.SettingKey.PointsForRedemption, newValue);
                }
            }

            this.txtPointsForRedemption.Text = newValue.ToString();

            // Update - button state
            if (newValue > MinRedeemPoints)
            {
                this.btnMinusRedeemPoints.IsEnabled = true;
            }
            else
            {
                this.btnMinusRedeemPoints.IsEnabled = false;
            }

            // Update + button state.
            if (newValue < MaxRedeemPoints)
            {
                this.btnPlusRedeemPoints.IsEnabled = true;
            }
            else
            {
                this.btnPlusRedeemPoints.IsEnabled = false;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
