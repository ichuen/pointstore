﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PointStore.Database;

namespace PointStore
{
    /// <summary>
    /// Interaction logic for StatsPage.xaml
    /// </summary>
    public partial class StatsPage : Page
    {
        public StatsPage()
        {
            InitializeComponent();

            ShowStats();
        }

        public void ShowStats() {
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime today = DateTime.Today.ToUniversalTime();
            long todayUnixSeconds = (long)(today - epoch).TotalSeconds;

            // Get total # of customers.
            long numCustomers = DatabaseManager.Instance.ExecuteScalarLong(
                "select count(*) from customer");

            // Get total # of points given for the current day.
            String sql = String.Format("SELECT sum(point_change) FROM customer_activity where action_type in (0, 1) and created >= datetime({0}, 'unixepoch')", todayUnixSeconds);
            object result = DatabaseManager.Instance.ExecuteScalar(sql);
            long pointsGivenToday = DatabaseManager.Instance.ExecuteScalarLong(sql);
            
            // Get total # of redemptions for the current day.
            sql = String.Format("SELECT count(*) FROM customer_activity where action_type in (2) and created >= datetime({0}, 'unixepoch')", todayUnixSeconds);
            long numberRedeemed = DatabaseManager.Instance.ExecuteScalarLong(sql);

            StringBuilder s = new StringBuilder();
            s.Append(String.Format("Total # of customers: {0}\n", numCustomers));
            s.Append(String.Format("Total points given today: {0}\n", pointsGivenToday));
            s.Append(String.Format("Total redemptions today: {0}\n", numberRedeemed));

            this.txtStats.Text = s.ToString();
        }
    }
}
