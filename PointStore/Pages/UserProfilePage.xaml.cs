﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PointStore.Database;
using PointStore.Enums;
using PointStore.Model;
using PointStore.Utils;

namespace PointStore
{
    /// <summary>
    /// Interaction logic for UserProfilePage.xaml
    /// </summary>
    public partial class UserProfilePage : Page
    {
        private int pointsForRedemption;

        private Customer customer;

        public UserProfilePage(Customer customer)
        {
            InitializeComponent();
            this.customer = customer;
            
            String p = Setting.GetSetting(Setting.SettingKey.PointsForRedemption);
            pointsForRedemption = int.Parse(p);
            
            UpdateDisplay();
        }

        private void UpdateDisplay()
        {
            this.txtPhoneNumber.Text = CommonUtils.GeneratePhoneString(customer.phoneNumber.ToString());
            this.txtPoints.Text = customer.points.ToString();
            this.txtTotalVisits.Text = customer.visits.ToString();
            this.txtCreated.Text = customer.created.ToLocalTime().ToString();
            this.txtUpdated.Text = customer.updated.ToLocalTime().ToString();
            this.btnRedeem.Content = String.Format("Redeem(-{0})", pointsForRedemption);

            // Only enable Redeem button if there are enough points.
            if (customer.points < pointsForRedemption)
            {
                this.btnRedeem.IsEnabled = false;
            }
            else
            {
                this.btnRedeem.IsEnabled = true;
            }

            // Disable Minus button if no points.
            if (customer.points <= 0)
            {
                this.btnMinusOnePoint.IsEnabled = false;
            }
            else
            {
                this.btnMinusOnePoint.IsEnabled = true;
            }
        }

        private void onAddOnePointClick(object sender, RoutedEventArgs e)
        {
            this.customer.UpdatePoints(1, ActionType.AddPoint);
            UpdateDisplay();
        }

        private void onMinusOnePointClick(object sender, RoutedEventArgs e)
        {
            this.customer.UpdatePoints(-1, ActionType.RemovePoint);
            UpdateDisplay();
        }

        private void onMinusTenPointsClick(object sender, RoutedEventArgs e)
        {
            this.customer.UpdatePoints(-pointsForRedemption, ActionType.RedeemPoints);
            UpdateDisplay();
        }
    }
}
