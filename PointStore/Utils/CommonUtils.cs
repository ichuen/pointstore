﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PointStore.Utils
{
    public class CommonUtils
    {
        public static string GeneratePhoneString(string phoneNumber) {
            int[] dashPositionIndexes = new int[] { 3, 6 };
            char dashDelimiter = '-';
            StringBuilder str = new StringBuilder();

            for (int i = 0; i < phoneNumber.Length; i++)
            {
                if (dashPositionIndexes.Contains(i)) {
                    str.Append(dashDelimiter);
                }
                str.Append(phoneNumber[i]);
            }

            return str.ToString();
        }
    }
}
