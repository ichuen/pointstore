﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;

using PointStore.Database;
using PointStore.Pages;

namespace PointStore
{
    /// <summary>
    /// Interaction logic for MainInterface.xaml
    /// </summary>
    public partial class MainInterfaceWindow : Window
    {
        private Window parent;

        public MainInterfaceWindow(Window parent)
        {
            InitializeComponent();
            this.parent = parent;
            this.mainInterfaceFrame.Navigate(new HomePage());
        }

        private void onClosed(object sender, EventArgs e)
        {
            this.parent.Show();
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            this.parent.Show();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var desktopWorkingArea = System.Windows.SystemParameters.WorkArea;
            this.Left = (desktopWorkingArea.Right - this.Width) / 2;
            this.Top = (desktopWorkingArea.Bottom - this.Height) / 2;
        }

        private void onBackButtonClick(object sender, RoutedEventArgs e)
        {
            this.mainInterfaceFrame.Navigate(new HomePage());
        }

        private void onViewStatsButtonClick(object sender, RoutedEventArgs e)
        {
            this.mainInterfaceFrame.Navigate(new StatsPage());
        }

        private void onSettingsButtonClick(object sender, RoutedEventArgs e)
        {
            this.mainInterfaceFrame.Navigate(new SettingsPage());
        }

        private void mainInterfaceFrame_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            if (e.Content is HomePage)
            {
                this.btnBack.Visibility = System.Windows.Visibility.Hidden;
            }
            else
            {
                this.btnBack.Visibility = System.Windows.Visibility.Visible;
            }

        }
    }
}
