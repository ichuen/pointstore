﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PointStore
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static int DefaultLocationLeftOffset = 30;
        private static int DefaultLocationTopOffset = 30;

        private MainInterfaceWindow mainInterfaceWindow;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void onButtonClick(object sender, RoutedEventArgs e)
        {
            mainInterfaceWindow = new MainInterfaceWindow(this);    
            mainInterfaceWindow.Show();
            this.Hide();
        }

        private void onWindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (mainInterfaceWindow != null && mainInterfaceWindow.IsLoaded)
            {
                mainInterfaceWindow.Close();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var desktopWorkingArea = System.Windows.SystemParameters.WorkArea;
            this.Left = desktopWorkingArea.Right - this.Width - DefaultLocationLeftOffset;
            this.Top = desktopWorkingArea.Bottom - this.Height - DefaultLocationTopOffset;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
    }
}
