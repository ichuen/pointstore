﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using PointStore;
using PointStore.Model;
using PointStore.Database;
using System.IO;

namespace PointStoreTests
{
    [TestClass]
    public class CustomerTests
    {
        private static String tempDatabaseFilePath = "testdb.sqlite";

        [TestInitialize]
        public void Setup()
        {
            // Cleanup if file exists.
            if (File.Exists(tempDatabaseFilePath))
            {
                File.Delete(tempDatabaseFilePath);
            }

            DatabaseManager.DatabaseFilePath = tempDatabaseFilePath;
        }

        [TestCleanup]
        public void Cleanup()
        {
            DatabaseManager.Instance.CloseDatabase();

            // Need to do this to have SQLite release the file.
            GC.Collect();
            GC.WaitForPendingFinalizers();

            if (File.Exists(tempDatabaseFilePath))
            {
                File.Delete(tempDatabaseFilePath);
            }
        }

        [TestMethod]
        public void CreateCustomers_EdgeCases()
        {
            CreateCustomerAndValidate(0, 1, 2);
            CreateCustomerAndValidate(1, 2, 3);
            CreateCustomerAndValidate(1000000000, 1000000001, 1000000002);
            CreateCustomerAndValidate(9999999999, 5000, 5001);
        }

        [TestMethod]
        public void CreateCustomers_Many()
        {
            for (int i = 1000000000; i < 1000001000; i++) {
                CreateCustomerHelper(i, i+1, i+2);
            }
        }

        private void CreateCustomerHelper(long phoneNumber, int points, int visits)
        {
            Customer c = new Customer(phoneNumber);
            c.points = points;
            c.visits = visits;
            c.Save();
        }

        private void ValidateCustomer(long phoneNumber, int points, int visits)
        {
            Customer c = Customer.GetByPhoneNumber(phoneNumber);
            Assert.AreEqual(phoneNumber, c.phoneNumber);
            Assert.AreEqual(points, c.points);
            Assert.AreEqual(visits, c.visits);
        }

        private void CreateCustomerAndValidate(long phoneNumber, int points, int visits)
        {
            CreateCustomerHelper(phoneNumber, points, visits);
            ValidateCustomer(phoneNumber, points, visits);
        }
    }
}
